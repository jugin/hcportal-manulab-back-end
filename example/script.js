/*
 *
 * Copyright 2018-2019 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 */

var tabStatistics = true;
var tabTextOp = false;
var tabCryptanalysis = false;

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

// var myBarChart;

window.onload = function() {
}

function changeToStatisticsUI() {
    document.getElementById("menu1").classList.add('w3-green');
    document.getElementById("contentStatistics").style.display = "block";
    document.getElementById("menu2").classList.remove('w3-green');
    document.getElementById("contentTextOp").style.display = "none";
    document.getElementById("menu3").classList.remove('w3-green');
    document.getElementById("contentCryptanalysis").style.display = "none";
    tabStatistics = true;
    tabTextOp = false;
    tabCryptanalysis = false;
}

function changeTotextOpUI() {
    document.getElementById("menu1").classList.remove('w3-green');
    document.getElementById("contentStatistics").style.display = "none";
    document.getElementById("menu2").classList.add('w3-green');
    document.getElementById("contentTextOp").style.display = "block";
    document.getElementById("menu3").classList.remove('w3-green');
    document.getElementById("contentCryptanalysis").style.display = "none";
    tabStatistics = false;
    tabTextOp = true;
    tabCryptanalysis = false;
}

function changeToCryptanalysisUI() {
    document.getElementById("menu1").classList.remove('w3-green');
    document.getElementById("contentStatistics").style.display = "none";
    document.getElementById("menu2").classList.remove('w3-green');
    document.getElementById("contentTextOp").style.display = "none";
    document.getElementById("menu3").classList.add('w3-green');
    document.getElementById("contentCryptanalysis").style.display = "block";
    tabStatistics = false;
    tabTextOp = false;
    tabCryptanalysis = true;
}

var hcText = {};

function dlHCPortalTxt() {
    var entryKeys = Object.keys(hcText);
    if (entryKeys.length) {
        return;
    }
    $.ajax({
        type: "GET",
        url: "https://manulab.hcportal.eu/api/dbadapters/rest/getRecordsFromHCPortal.php?type=text",
        crossDomain: true,
        contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
        dataType: "json",
        success: function (data) {
            var records = JSON.parse(JSON.stringify(data));
            // console.log(records);
            if (records != null) {
                var dropdown = document.getElementById('dropDownHCTxt');
                for (var i = 0; i < records.length; i++) {
                    var r = records[i];
                    var name = r.name;
                    //
                    var item = document.createElement('button');
                    item.className += 'dropdown-item';
                    var iid = 'hc_txt_i_' + i;
                    item.setAttribute('iid', iid);
                    hcText[iid] = r;
                    item.innerHTML = name;
                    item.addEventListener('click', function () {
                        var iid = this.getAttribute('iid');
                        if (tabStatistics) {
                            var p1 = document.getElementById("page1");
                            if (p1.value == "") {
                                document.getElementById("page1").value = hcText[iid].text;
                            } else {
                                addPage();
                                document.getElementById("page" + pages).value = hcText[iid].text;
                                document.getElementById("span" + pages).value = hcText[iid].name;
                            }
                        } else if (tabTextOp) {
                            document.getElementById("pageTO").value = hcText[iid].text;
                        } else if (tabCryptanalysis) {
                            document.getElementById("pageCA").value = hcText[iid].text;
                        }
                    }, false);
                    dropdown.appendChild(item);

                }
            }
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}


var pages = 1;

function addPage() {
    pages++; // increment page to get a unique ID for the new element

    var html = '<div class="input-group mb-3">' +
        '<div class="input-group-prepend"><button type="button" class="btn btn-danger" onclick="removePage(' + pages + ')">X</button>' +
        '<span id="span' + pages + '" class="input-group-text">Page ' + pages + '</span>' +
        '</div>' +
        '<textarea rows="5" cols="50" id="page' + pages + '" class="form-control" name="description"></textarea>' +
        '</div>';

    addElementId('append-page', 'div', 'page-' + pages, html);

}

function clearPages(){
    for(let i=2; i <= pages; i++){
        removePage(i);
    }
    document.getElementById("page1").value = "";
    pages = 1;
}

function addElementId(parentId, elementTag, elementId, html) {
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('id', elementId);
    newElement.innerHTML = html;
    p.appendChild(newElement);
}

function addElementAndValue(parentId, elementTag, elementValue, html) {
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('value', elementValue);
    newElement.innerHTML = html;
    p.appendChild(newElement);
}

function removePage(pageID) {
    removeElement('page-' + pageID);
}

function removeElement(elementId) {
    var element = document.getElementById(elementId);
    element.remove();
}

function init() {
    changeToStatisticsUI();
}

function pagesToJSONString() {
    var obj = {};

    obj["pages"] = [];
    for (var i = 1; i <= pages; i++) {
        var pageElement = document.getElementById("page" + i);
        //console.log("tagKey: "+"inputGroup-tag"+i+"-new");
        if (!(pageElement === null || pageElement.value == "")) {
            var page = {};
            page["id"] = "Page " + i;
            page["text"] = pageElement.value;
            obj["pages"].push(page);
        }
    }

    return obj;
}

function cleanup() {
    // remove if avail
    if (document.getElementById("gl_table_detail") != null) {
        removeElement('gl_table_detail');
    }
    if (document.getElementById("fr_table") != null) {
        removeElement('fr_table');
    }
    if (document.getElementById("dist_table") != null) {
        removeElement('dist_table');
    }
}

function getFrequency() {
    cleanup();
    var json = pagesToJSONString();
    //
    var delimiter = document.getElementById("fr_delimiter").value;
    if (!(delimiter === null || delimiter == "")) {
        json["delimiter"] = delimiter;
    }
    var ngram = document.getElementById("fr_ngram").value;
    if (!(ngram === null || ngram == "")) {
        json["n"] = ngram;
    }
    if (document.getElementById("fr_type").checked == true) {
        json["relative"] = 1;
    } else {
        json["relative"] = 0;
    }

    //
    json = JSON.stringify(json);
    //console.log(json);

    $.ajax({
        type: "POST",
        url: "https://manulab.hcportal.eu/api/text/rest/getFrequency.php",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
        dataType: "json",
        success: function (data) {
            //console.log(data);

            var keys = Object.keys(data.result);
            var text = "";
            var i;
            for (i = 0; i < keys.length; i++) {
                text += keys[i] + ": \n";
                var value = data.result[keys[i]];

                //
                var inner_keys = Object.keys(value);
                var j;
                for (j = 0; j < inner_keys.length; j++) {
                    text += inner_keys[j] + "-" + value[inner_keys[j]] + "; ";
                }
                text += "\n";

            }
            //console.log(text);

            document.getElementById("fr_result").value = text;
            // additionary table
            if (document.getElementById("fr_chart_checkbox").checked == true) {
                document.getElementById('chartContainer').style.display = "block";

                // add new canvas
                // document.getElementById('chartContainer').innerHTML = '<canvas id="canvas"></canvas>'; // replaces the inner HTML of #someBox to a canvas

                var chartInnerHtml = '<div>';
                var keys = Object.keys(data.result);
                for (var i = 0; i < keys.length; i++) {
                    chartInnerHtml = chartInnerHtml + '<canvas id="canvas' + i + '"></canvas>';
                }
                chartInnerHtml = chartInnerHtml + '</div>';
                document.getElementById('chartContainer').innerHTML = chartInnerHtml;

                for (var i = 0; i < keys.length; i++) {
                   // if (keys[i] == "all") {
                        var value = data.result[keys[i]];
                        var chartData = createFrequencyChart(value);
                        //
                        var ctx = document.getElementById('canvas' + i ).getContext('2d');
                        var myBarChart = new Chart(ctx, {
                            type: 'bar',
                            data: chartData,
                            options: {
                                responsive: true,
                                legend: {
                                    position: 'top',
                                },
                                title: {
                                    display: true,
                                    text: 'Frequency chart - ' +  keys[i]
                                }
                            }
                        });
                    }
               // }
                // var chartData = createFrequencyChart(data.result);
                // console.log(table);
                // var ctx = document.getElementById('canvas').getContext('2d');
                // var myBarChart = new Chart(ctx, {
                //     type: 'bar',
                //     data: chartData,
                //     options: {
                //         responsive: true,
                //         legend: {
                //             position: 'top',
                //         },
                //         title: {
                //             display: true,
                //             text: 'Frequency chart'
                //         }
                //     }
                // });
            } else{
                document.getElementById('chartContainer').style.display = "none";
                document.getElementById('chartContainer').innerHTML = "";

            }

            if (document.getElementById("fr_table_checkbox").checked == true) {
                var table = createFrequencyTable(data.result);
                var currentDiv = document.getElementById("fr_container");
                currentDiv.appendChild(table);
            }
            //}
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function getEntropy() {
    cleanup();
    var json = pagesToJSONString();
    //
    var delimiter = document.getElementById("entropy_delimiter").value;
    if (!(delimiter === null || delimiter == "")) {
        json["delimiter"] = delimiter;
    }
    //
    json = JSON.stringify(json);
    //console.log(json);

    $.ajax({
        type: "POST",
        url: "https://manulab.hcportal.eu/api/text/rest/getShannonsEntropy.php",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
        dataType: "json",
        success: function (data) {
            //console.log(data);

            var keys = Object.keys(data.result);
            var text = "";
            var i;
            for (i = 0; i < keys.length; i++) {
                var value = data.result[keys[i]];
                text += keys[i] + " - " + value + "\n";
            }
            //console.log(text);

            document.getElementById("entropy_result").value = text;
            //}
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function getIC() {
    cleanup();
    var json = pagesToJSONString();
    //
    var delimiter = document.getElementById("ic_delimiter").value;
    if (!(delimiter === null || delimiter == "")) {
        json["delimiter"] = delimiter;
    }
    if (document.getElementById("ic_normalize").checked == true) {
        json["normalize"] = 1;
    } else {
        json["normalize"] = 0;
    }

    if (document.getElementById("ic_approx").checked == true) {
        json["approximate"] = 1;
    } else {
        json["approximate"] = 0;
    }
    //
    json = JSON.stringify(json);
    //console.log(json);

    $.ajax({
        type: "POST",
        url: "https://manulab.hcportal.eu/api/text/rest/getIndexOfCoincidence.php",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
        dataType: "json",
        success: function (data) {
            //console.log(data);

            var keys = Object.keys(data.result);
            var text = "";
            var i;
            for (i = 0; i < keys.length; i++) {
                var value = data.result[keys[i]];
                text += keys[i] + " - " + value + "\n";
            }
            //console.log(text);

            document.getElementById("ic_result").value = text;
            //}
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function removeAccents() {
    cleanup();
    var pageElement = document.getElementById("pageTO");
    if (!(pageElement === null || pageElement.value == "")) {
        var json = {};
        json["text"] = pageElement.value;
        //
        if (document.getElementById("ra_space").checked == true) {
            json["space"] = 1;
        } else {
            json["space"] = 0;
        }
        //
        json = JSON.stringify(json);
        //console.log(json);

        $.ajax({
            type: "POST",
            url: "https://manulab.hcportal.eu/api/text/rest/removeAccents.php",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
            dataType: "json",
            success: function (data) {
                //console.log(data);
                document.getElementById("ra_result").value = data.text;
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }
}

function guessLanguage() {
    cleanup();
    //
    var pageElement = document.getElementById("pageCA");
    if (!(pageElement === null || pageElement.value == "")) {
        var json = {};
        json["text"] = pageElement.value;
        //
        json = JSON.stringify(json);
        //console.log(json);

        $.ajax({
            type: "POST",
            url: "https://manulab.hcportal.eu/api/text/rest/guessLanguage.php",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
            dataType: "json",
            success: function (data) {
                // console.log(data);
                var result = data.result;
                var parameters = data.parameters;
                // console.log(result);
                if (result != null && parameters != null) {
                    var firstKey = Object.keys(result)[0];
                    var firstVal = result[firstKey];
                    var resultString = 'The most probable language is: ' + firstKey + ', with difference of IC: ' + firstVal + '.';
                    if (document.getElementById("gl_showDetails").checked == true) {
                        resultString = resultString + '\n' + 'Input IC: ' + parameters["input ic"] + '.';
                        resultString = resultString + '\n' + firstKey + ' IC: ' + parameters['reference IC values'][firstKey] + '.';

                    }
                    document.getElementById("gl_result").value = resultString;
                    // additionary table
                    if (document.getElementById("gl_showDetails").checked == true) {
                        var table = createLanguagesTable(parameters['reference IC values'], result);
                        var currentDiv = document.getElementById("gl_container");
                        currentDiv.appendChild(table);
                    }
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }
}

function vowelDetection() {
    cleanup();
    // get the selected options
    var em = document.getElementById("vdMethod");
    var method = em.options[em.selectedIndex].value;
    // var el = document.getElementById("vdLang");
    // var lang = el.options[el.selectedIndex].value;
    //
    var pageElement = document.getElementById("pageCA");
    if (!(pageElement === null || pageElement.value == "")) {
        var json = {};
        json["text"] = pageElement.value;
        json["vowelDetectionMethod"] = method;
        // json["language"] = lang;
        //
        json = JSON.stringify(json);
        //console.log(json);

        $.ajax({
            type: "POST",
            url: "https://manulab.hcportal.eu/api/text/rest/guessVowels.php",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
            dataType: "json",
            success: function (data) {
                var result = data.result;
                if (result != null) {

                    var resultString = 'Vowels are: ';
                    for (var i = 0; i < result.length; i++) {
                        resultString = resultString + result[i];
                        if (i < result.length - 1) {
                            resultString = resultString + ', ';
                        }
                    }
                    document.getElementById("vd_result").value = resultString;

                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }
}

function anagramDetection() {
    cleanup();
    //
    var firstElement = document.getElementById("a_first");
    var secondElement = document.getElementById("a_second");

    if (firstElement === null || secondElement === null || firstElement.value == "" || secondElement.value == null) {
        return;
    }
    var json = {};
    json["first"] = firstElement.value;
    json["second"] = secondElement.value;
    //
    json = JSON.stringify(json);

    $.ajax({
        type: "POST",
        url: "https://manulab.hcportal.eu/api/text/rest/detectAnagrams.php",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
        dataType: "json",
        success: function (data) {
            var result = data.result;
            if (result != null) {
                document.getElementById("ad_result").value = firstElement.value + ' is anagram of ' + secondElement.value + '? - ' + result;
            }
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });

}

function getDistances() {
    cleanup();
    var json = pagesToJSONString();
    //
    var delimiter = document.getElementById("dist_delimiter").value;
    if (!(delimiter === null || delimiter == "")) {
        json["delimiter"] = delimiter;
    }
    //
    json = JSON.stringify(json);
    //console.log(json);

    $.ajax({
        type: "POST",
        url: "https://manulab.hcportal.eu/api/text/rest/getDistances.php",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",// if not working, change to contentType: "application/x-www-form-urlencoded"
        dataType: "json",
        success: function (data) {
            var keys = Object.keys(data.result); // pages
            var text = "";
            for (var i = 0; i < keys.length; i++) {
                text += keys[i] + ": \n";
                var value = data.result[keys[i]];
                var inner_keys = Object.keys(value); // elements
                for (var j = 0; j < inner_keys.length; j++) {
                    var container = value[inner_keys[j]];
                    text += "element: " + inner_keys[j];
                    text += ", avg: " + container['avg'];
                    text += ", min: " + container['min'];
                    text += ", max: " + container['max'];
                    text += "\n";
                }
                text += "\n";

            }
            //console.log(text);

            document.getElementById("dist_result").value = text;
            // additionary table
            if (document.getElementById("dist_table_checkbox").checked == true) {
                var table = createDistancesTable(data.result);
                var currentDiv = document.getElementById("dist_container");
                currentDiv.appendChild(table);
            }

        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function createLanguagesTable(refs, diffs) {
    var table = document.createElement('table');
    var keys = Object.keys(diffs);

    var headers = document.createElement('tr');
    var header1 = document.createElement('th');
    header1.textContent = 'Language';
    headers.appendChild(header1);
    var header2 = document.createElement('th');
    header2.textContent = 'Reference IC';
    headers.appendChild(header2);
    var header3 = document.createElement('th');
    header3.textContent = 'IC difference';
    headers.appendChild(header3);
    table.appendChild(headers);


    for (var i = 0; i < keys.length; i++) {
        var row = document.createElement('tr');
        var cellLang = document.createElement('td');
        cellLang.textContent = keys[i];
        row.appendChild(cellLang);

        var cellRef = document.createElement('td');
        cellRef.textContent = refs[keys[i]];
        row.appendChild(cellRef);

        var cellDiff = document.createElement('td');
        cellDiff.textContent = diffs[keys[i]];
        row.appendChild(cellDiff);

        table.appendChild(row);
    }
    table.setAttribute('id', 'gl_table_detail');

    return table;
}

function createFrequencyChart(value){

    var color = Chart.helpers.color;

    var labels;
    var datasets = [];
    var idx = 0;
    var labelTxt = "";
    var ngram = document.getElementById("fr_ngram").value;
    if (!(ngram === null || ngram == "")) {
        labelTxt = ngram+"-gram frequencies";
    } else {
        labelTxt = "n-gram frequencies";
    }


    let dataset = {
        label : labelTxt,
        backgroundColor : color(window.chartColors.green).alpha(0.5).rgbString(),
        borderColor : window.chartColors.green,
        borderWidth : 1
    };
    var inner_keys = Object.keys(value);
    labels = [];
    let chartData = [];
    for (var j = 0; j < inner_keys.length; j++) {
        var key_i = inner_keys[j];
        var value_i = value[inner_keys[j]];
        labels[j] = key_i;
        chartData[j] = value_i;
    }
    dataset["data"] = chartData;
    datasets[idx++] = dataset;


    var barChartData = {
        labels: labels,
        datasets: datasets
    };

    return barChartData;

    // var color = Chart.helpers.color;
    //
    // var labels;
    // var datasets = [];
    // var idx = 0;
    // var keys = Object.keys(data);
    // var labelTxt = "";
    // var ngram = document.getElementById("fr_ngram").value;
    // if (!(ngram === null || ngram == "")) {
    //     labelTxt = ngram+"-gram frequencies";
    // } else {
    //     labelTxt = "n-gram frequencies";
    // }
    //
    // //
    //
    // var lastKey = keys[keys.length - 1];
    // // for (var i = 0; i < keys.length; i++) {
    // //    if(keys[i] == "all") {
    // //         var value = data[keys[i]];
    //         var value = data[lastKey];
    //         let dataset = {
    //             label : labelTxt,
    //             backgroundColor : color(window.chartColors.green).alpha(0.5).rgbString(),
    //             borderColor : window.chartColors.green,
    //             borderWidth : 1
    //         };
    //         var inner_keys = Object.keys(value);
    //         labels = [];
    //         let chartData = [];
    //         for (var j = 0; j < inner_keys.length; j++) {
    //             var key_i = inner_keys[j];
    //             var value_i = value[inner_keys[j]];
    //             labels[j] = key_i;
    //             chartData[j] = value_i;
    //         }
    //         dataset["data"] = chartData;
    //         datasets[idx++] = dataset;
    //     // }
    // // }
    //
    // var barChartData = {
    //     labels: labels,
    //     datasets: datasets
    // };
    //
    // return barChartData;
}

function createFrequencyTable(data) {
    var table = document.createElement('table');
    var keys = Object.keys(data);

    for (var i = 0; i < keys.length; i++) {
        var value = data[keys[i]];
        // for all page add headers
        var headers = document.createElement('tr');
        var header1 = document.createElement('th');
        header1.textContent = 'Page';
        headers.appendChild(header1);
        var header2 = document.createElement('th');
        header2.textContent = 'Element';
        headers.appendChild(header2);
        var header3 = document.createElement('th');
        header3.textContent = 'Frequency';
        headers.appendChild(header3);
        table.appendChild(headers);

        var inner_keys = Object.keys(value);
        for (var j = 0; j < inner_keys.length; j++) {
            var row = document.createElement('tr');

            var cellPage = document.createElement('td');
            cellPage.textContent = keys[i];
            row.appendChild(cellPage);

            var cellElement = document.createElement('td');
            cellElement.textContent = inner_keys[j];
            row.appendChild(cellElement);

            var cellFreq = document.createElement('td');
            cellFreq.textContent = value[inner_keys[j]];
            row.appendChild(cellFreq);

            table.appendChild(row);
        }
    }

    table.setAttribute('id', 'fr_table');
    return table;
}


function createDistancesTable(data) {
    var table = document.createElement('table');
    var keys = Object.keys(data);

    for (var i = 0; i < keys.length; i++) {
        var value = data[keys[i]];
        // for all page add headers
        var headers = document.createElement('tr');
        var header1 = document.createElement('th');
        header1.textContent = 'Page';
        headers.appendChild(header1);
        var header2 = document.createElement('th');
        header2.textContent = 'Element';
        headers.appendChild(header2);
        var header3 = document.createElement('th');
        header3.textContent = 'Avg. distance';
        headers.appendChild(header3);
        var header4 = document.createElement('th');
        header4.textContent = 'Min. distance';
        headers.appendChild(header4);
        var header5 = document.createElement('th');
        header5.textContent = 'Max. distance';
        headers.appendChild(header5);
        table.appendChild(headers);

        //
        var text = "";
        for (var i = 0; i < keys.length; i++) {
            text += keys[i] + ": \n";
            var value = data[keys[i]];
            var inner_keys = Object.keys(value); // elements
            for (var j = 0; j < inner_keys.length; j++) {
                var row = document.createElement('tr');

                var cellPage = document.createElement('td');
                cellPage.textContent = keys[i];
                row.appendChild(cellPage);

                var cellElement = document.createElement('td');
                cellElement.textContent = inner_keys[j];
                row.appendChild(cellElement);

                var container = value[inner_keys[j]];

                var cellAvg = document.createElement('td');
                cellAvg.textContent = container['avg'];
                row.appendChild(cellAvg);

                var cellMin = document.createElement('td');
                cellMin.textContent = container['min'];
                row.appendChild(cellMin);

                var cellMax = document.createElement('td');
                cellMax.textContent = container['max'];
                row.appendChild(cellMax);

                table.appendChild(row);
            }
        }
    }
    table.setAttribute('id', 'dist_table');
    return table;
}



// function frToggle(ch) {
//     if (ch.id == "fr_chart_checkbox" ) {
//         if(ch.checked){
//             document.getElementById("fr_table_checkbox").checked = false;
//         }
//     } else {
//         if(ch.checked){
//             document.getElementById("fr_chart_checkbox").checked = false;
//         }
//     }
// }
