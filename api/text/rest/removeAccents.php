<?php
/*
 *
 * Copyright 2018 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 */
/**
 * @api {post} /api/text/rest/removeAccents.php  Remove accents
 * @apiName removeAccents
 * @apiGroup TextOperations
 *
 * @apiHeaderExample  Header-Example:
 * {
 *      Accept: application/json
 *      Accept-Encoding:gzip, deflate
 *      Connection: keep-alive
 *      Content-Type: application/json; charset=UTF-8
 * }
 *
 * @apiHeaderExample  Header-Example (simple):
 * {
 *      Content-Type: application/x-www-form-urlencoded // "if application/json; charset=UTF-8 is not working"
 * }
 *
 * @apiParam {String} text    Input text (see the request example).
 * @apiParam {Boolean} [space=0]   Keep space flag with default value 0. If the value is set to 1, the space characters are not removed from the text.
 * @apiParamExample {json} Request-Example:
 *     {
 *          "space" : 1,
 *          "text" : "Lorem ipsum générateur avec accents et caractères spéciaux français."
 *      }
 *
 * @apiSuccess {String/JSON} response Lower-case text without accents and punctuations.
 * @apiError {String/JSON} response Error message in a JSON format.
 *
 * @apiSuccessExample Success-Response:
 *    {
 *          "text": resulting text,
 *          "parameters": {
 *               "keepSpace": <flag value "YES" or "NO">
 *          }
 *    }
 *
 * * @apiErrorExample Error-Response:
 *     {
 *          "message" : <error messsage>
 *     }
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../objects/textoperations.php';


$json_data = file_get_contents("php://input");

if ($json_data === false) {
    http_response_code(412); // precondition failed code
    echo json_encode(
        array("message" => "Input error. Http body not set.")
    );
    die();
} else {
    $json_data = json_decode($json_data);
    // input text
    if (!isset($json_data->text)) {
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. No input text to process.")
        );
        die();
    } else {
        $text = $json_data->text;
    }

    // other parameters
    if (isset($json_data->space)) {
        $space = $json_data->space;
    } else {
        // default value
        $space = 0;
    }

    $txtop = new TextOperations();
    $result = array();

    $result["text"] = $txtop->convertToTelegraphic($text, $space);

    $parameters = array();
    $parameters['space'] = ($space)?("YES"):("NO");
    $result['parameters'] = $parameters;

    http_response_code(200);
    echo json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

}

?>