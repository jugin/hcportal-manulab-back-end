<?php
/*
 *
 * Copyright 2018 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 */
/**
 * @api {post} /api/text/rest/patternSearch.php  Pattern search
 * @apiName patternSearch
 * @apiGroup TextStatistics
 *
 * @apiHeaderExample  Header-Example:
 * {
 *      Accept: application/json
 *      Accept-Encoding:gzip, deflate
 *      Connection: keep-alive
 *      Content-Type: application/json; charset=UTF-8
 * }
 *
 * @apiHeaderExample  Header-Example (simple):
 * {
 *      Content-Type: application/x-www-form-urlencoded // "if application/json; charset=UTF-8 is not working"
 * }
 *
 * @apiParam {String/JSON} pages    Pages (see the request example).
 * @apiParam {String} pattern  Search pattern.
 * @apiParamExample {json} Request-Example:
 *     {
 *          "pattern" : "it",
 *          "pages" :
 *              [
 *                  { "id": "1", "text": "Lorem ipsum dolor sit amet," },
 *                  { "id": "2", "text": "consectetur adipiscing elit" }
 *              ]
 *      }
 *
 * @apiSuccess {String/JSON} response Starting positions of the searched pattern by page.
 * @apiError {String/JSON} response Error message in a JSON format.
 *
 * @apiSuccessExample Success-Response:
 *    {
 *          "result": {
 *              "<page id>": [
 *                  position_1,
 *                  position_2,
 *                  ...
 *                  position_n
 *              ],
 *              "<page id>": [
 *                  position_1
 *              ],
 *              ...
 *          },
 *          "parameters": {
 *               "searched pattern" : <pattern>
 *               "number of pages": <number of pages>
 *          }
 *    }
 *
 * * @apiSuccessExample Success-Response:
 *    {
 *          // If the pattern is nout found, the "positions" parameter is not set.
 *          "parameters": {
 *               "searched pattern" : <pattern>
 *               "number of pages": <number of pages>
 *          }
 *    }
 *
 * * @apiErrorExample Error-Response:
 *     {
 *          "message" : <error messsage>
 *     }
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../objects/statistics.php';


$json_data = file_get_contents("php://input");

if ($json_data === false) {
    http_response_code(412); // precondition failed code
    echo json_encode(
        array("message" => "Input error. Http body not set.")
    );
    die();
} else {
    $json_data = json_decode($json_data);
    // input text
    if (!isset($json_data->pages)) {
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. No input text to process.")
        );
        die();
    } else {
        $pages = array();
        if (sizeof($json_data->pages) > 0 ) {
            foreach($json_data->pages as $page_item) {
                $pages[$page_item->id] = $page_item->text;
            }
        }
        //print_r($pages);
    }

    if(!isset($json_data->pattern)){
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. Pattern patrameter not set.")
        );
        die();
    } else {
        $pattern = $json_data->pattern;
    }
    // other parameters
    $ngram = 1;
    $delimiter = '';

    $stat = new Statistics($pages, $ngram, $delimiter);
    $result = array();
    $patterns = $stat->patternSearch($pattern);
    if(!empty($patterns)) {
        $result["result"] = $patterns;
    }

    $parameters = array();
    $parameters['searched pattern'] = $pattern;

    if(is_array($pages)){
        $parameters['number of pages'] = sizeof($pages);
    } else {
        $parameters['number of pages'] = 1;
    }

    $result['parameters'] = $parameters;
    http_response_code(200);
    echo json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

}

?>


