<?php
/*
 *
 * Copyright 2019 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 */
/**
 * @api {post} /api/text/rest/detectAnagrams.php  Anagram detection
 * @apiName detectAnagrams
 * @apiGroup Cryptanalysis
 *
 * @apiHeaderExample  Header-Example:
 * {
 *      Accept: application/json
 *      Accept-Encoding:gzip, deflate
 *      Connection: keep-alive
 *      Content-Type: application/json; charset=UTF-8
 * }
 *
 * @apiHeaderExample  Header-Example (simple):
 * {
 *      Content-Type: application/x-www-form-urlencoded // "if application/json; charset=UTF-8 is not working"
 * }
 *
 * @apiParam {String} first    Input text A.
 * @apiParam {String} second   Input text B.
 * @apiParamExample {json} Request-Example:
 *     {
 *          "first" : "William Shakespeare",
 *          "second" : "I am a weakish speller"
 *
 *      }
 *
 * @apiSuccess {String/JSON} response If A is anagram of B.
 * @apiError {String/JSON} response Error message in a JSON format.
 *
 * @apiSuccessExample Success-Response:
 *    {
 *           "result": <flag value "YES" or "NO">
 *    }
 *
 * @apiErrorExample Error-Response:
 *     {
 *          "message" : <error messsage>
 *     }
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../objects/anagrams.php';


$json_data = file_get_contents("php://input");

if ($json_data === false) {
    http_response_code(412); // precondition failed code
    echo json_encode(
        array("message" => "Input error. Http body not set.")
    );
    die();
} else {
    $json_data = json_decode($json_data);
    // input text
    if (!isset($json_data->first) || !isset($json_data->second)) {
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. No input text to process.")
        );
        die();
    } else {
        $first = $json_data->first;
        $second = $json_data->second;
    }

    $obj = new Anagrams(null);
    $res = $obj->detectAnagrams($first,$second);
    $response['result'] = ($res)?("YES"):("NO");

    http_response_code(200);
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

}

?>