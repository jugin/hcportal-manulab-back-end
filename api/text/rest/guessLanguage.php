<?php
/*
 *
 * Copyright 2018 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 */
/**
 * @api {post} /api/text/rest/guessLanguage.php  Language guess
 * @apiName guessLanguage
 * @apiGroup Cryptanalysis
 *
 * @apiHeaderExample  Header-Example:
 * {
 *      Accept: application/json
 *      Accept-Encoding:gzip, deflate
 *      Connection: keep-alive
 *      Content-Type: application/json; charset=UTF-8
 * }
 *
 * @apiHeaderExample  Header-Example (simple):
 * {
 *      Content-Type: application/x-www-form-urlencoded // "if application/json; charset=UTF-8 is not working"
 * }
 *
 * @apiParam {String} text    Input text (see the request example).
 * @apiParamExample {json} Request-Example:
 *     {
 *          "text" : "Lorem ipsum générateur avec accents et caractères spéciaux français."
 *      }
 *
 * @apiSuccess {String/JSON} response List of languages, sorted by their probability (input IC difference from the reference language IC value).
 * @apiError {String/JSON} response Error message in a JSON format.
 *
 * @apiSuccessExample Success-Response:
 *    {
 *          "result": [
 *              { <language 1> : <IC difference 1> },
 *              { <language 2> : <IC difference 2> },
 *              ...
 *          ],
 *          "parameters": {
 *               "input ic": <IC value>,
 *               "reference IC values":[
 *                  { <language 1> : <ref. IC> },
 *                  { <language 2> : <ref. IC> },
 *              ...
 *              ]
 *          }
 *    }
 *
 * * @apiErrorExample Error-Response:
 *     {
 *          "message" : <error messsage>
 *     }
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../objects/cryptanalysis.php';


$json_data = file_get_contents("php://input");

if ($json_data === false) {
    http_response_code(412); // precondition failed code
    echo json_encode(
        array("message" => "Input error. Http body not set.")
    );
    die();
} else {
    $json_data = json_decode($json_data);
    // input text
    if (!isset($json_data->text)) {
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. No input text to process.")
        );
        die();
    } else {
        $text = $json_data->text;
    }

    $lang = 'English';
    $ca = new Cryptanalysis($text, $lang);
    $langs = $ca->guessTheUsedLanguage();

    $parameters = array();
    $parameters['input ic'] = $langs['IC'];
    $parameters['reference IC values'] = $langs['reference'];
    unset($langs['IC']);
    unset($langs['reference']);

    $result['result'] = $langs;
    $result['parameters'] = $parameters;

    http_response_code(200);
    echo json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

}

?>