<?php
/*
 *
 * Copyright 2018 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 */
/**
 * @api {post} /api/text/rest/getDistances.php  Text element distances
 * @apiName getElementDistances
 * @apiGroup TextStatistics
 *
 * @apiHeaderExample  Header-Example:
 * {
 *      Accept: application/json
 *      Accept-Encoding:gzip, deflate
 *      Connection: keep-alive
 *      Content-Type: application/json; charset=UTF-8
 * }
 *
 * @apiHeaderExample  Header-Example (simple):
 * {
 *      Content-Type: application/x-www-form-urlencoded // "if application/json; charset=UTF-8 is not working"
 * }
 *
 * @apiParam {String/JSON} pages    Pages (see the request example).
 * @apiParam {String} [delimiter='']  Delimiter with default value '' (empty string - split by chars).
 * @apiParamExample {json} Request-Example:
 *     {
 *          "delimiter" : " ",
 *          "pages" :
 *              [
 *                  { "id": "1", "text": "Lorem ipsum dolor sit amet," },
 *                  { "id": "2", "text": "consectetur adipiscing elit" }
 *              ]
 *      }
 *
 * @apiSuccess {String/JSON} response Elements distance statistics.
 * @apiError {String/JSON} response Error message in a JSON format.
 *
 * @apiSuccessExample Success-Response:
 *    {
 *           "result": {
 *              "<page id>": {
 *                      "<text element>": {
 *                          "avg": <average distance>,
 *                          "min": <minimal distance>,
 *                          "max": <maximal distance>,
 *                          "distances": [ <distance between 1st and 2nd occurrence>,
 *                                         <distance between 2nd and 3rd occurrence>,
 *                                         ...
 *                                       ]
 *                          },
 *                      "<text element>": {
 *                          ...
 *                          },
 *                      ...
 *                      },
 *               "<page id>": {
 *                   ...
 *               },
 *              ...
 *              "all": { ... }
 *              },
 *           "parameters": {
 *              "delimiter": <delimiter>, used delimiter
 *           }
 *    }
 *
 * @apiErrorExample Error-Response:
 *     {
 *          "message" : <error messsage>
 *     }
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../objects/statistics.php';


$json_data = file_get_contents("php://input");

if ($json_data === false) {
    http_response_code(412); // precondition failed code
    echo json_encode(
        array("message" => "Input error. Http body not set.")
    );
    die();
} else {
    $json_data = json_decode($json_data);
    // input text
    if (!isset($json_data->pages)) {
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. No input text to process.")
        );
        die();
    } else {
        $pages = array();
        if (sizeof($json_data->pages) > 0 ) {
            foreach($json_data->pages as $page_item) {
                $pages[$page_item->id] = $page_item->text;
            }
        }
    }

    $ngram = 1;

    if (isset($json_data->delimiter)) {
        $delimiter = $json_data->delimiter;
    } else {
        // default value
        $delimiter = '';
    }

    $stat = new Statistics($pages, $ngram, $delimiter);
    $result = $stat->getLetterDistances();

    $parameters = array();
    $parameters['delimiter'] = $delimiter;


    $response = array();
    $response['result'] = $result;
    $response['parameters'] = $parameters;

    http_response_code(200);
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

}

?>