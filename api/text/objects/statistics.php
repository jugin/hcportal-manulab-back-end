<?php
/*
 *
 * Copyright 2018 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 * Date: 07/11/2018
 */

class Statistics {

    private $pages;
    private $n;
    private $delimiter;

    // constructor with required parameters
    public function __construct($pages, $n, $delimiter){
        $this->pages = $pages;
        $this->n = $n;
        $this->delimiter = $delimiter;
        if(!isset($this->delimiter) || $this->delimiter == null) {
            $this->delimiter = "";
        }
    }

    /*
     *  Calculates the letter distances.
     *  The inputs are set by the constructor.
     *  Parameter n is force set to 1.
     *
     *  Returns letter distances.
     */
    public function getLetterDistances(){
        $result = array();
        $allInOne = "";
        $this->n = 1;

        // by page
        foreach($this->pages as $page => $page_value) {
            // 1. get the unique symbols from frequencies
            $frequency = $this->countNGram($page_value);
            $unique = array_unique($frequency);
            $distances = $this->findDistances($page_value, $unique);
            if (!empty($distances)) {
                $result[$page] = $distances;
            }
        }

        if(sizeof($this->pages) > 1){
            foreach($this->pages as $page => $page_value) {
                $allInOne .= $page_value;
                $allInOne .= $this->delimiter;
            }
            $allInOne = trim($allInOne);
            // all in one
            $frequency = $this->countNGram($allInOne);
            $unique = array_unique($frequency);
            $distances = $this->findDistances($allInOne, $unique);
            if (!empty($distances)) {
                $result['all'] = $distances;
            }
        }
        return $result;
    }


    /**
     * Helper method used by the getLetterDistances() method.
     * @param $text
     * @param $symbols
     * @return array Returns the distances for a specific letter.
     */
    public function findDistances($text, $symbols){
        $result = array();
        // split by delimitter
        if($this->delimiter == ""){
            $pieces_all = str_split($text);
        } else {
            $pieces_all = explode($this->delimiter, $text);
        }

        $positions = array();
        for ($i = 0; $i < count($pieces_all); $i++) {
            $piece = $pieces_all[$i];
            if(!array_key_exists($piece, $positions)){
                $positions[$piece] = array();
            }
            array_push($positions[$piece],$i);
        }

        foreach($positions as $key => $pos_array) {
            if(count($pos_array) > 1){
                // avg min max
                $res_key = array();
                // find the distances
                $distances = array();
                for($i=1; $i < count($pos_array); $i++){
                    $distance = $pos_array[$i] - $pos_array[$i-1];
                    array_push($distances, $distance);
                }
                // calcuate the statistics
                $res_key['avg'] = number_format(array_sum($distances) / count($distances),4);
                $res_key['min'] = min($distances);
                $res_key['max'] = max($distances);
                $res_key['distances'] = $distances;
                $result[$key] = $res_key;
            } // end if
        }

        return $result;
    }


    /**
     * Search for a specific pattern.
     * Parameter n is force set to 1.
     * @param $pattern
     * @return array all the starting positions of the searched pattern in the input text.
     */
    public function patternSearch($pattern){
        $result = array();
        $allInOne = "";
        $this->n = 1;

        // by page
        foreach($this->pages as $page => $page_value) {
            $positions = $this->findPosition($page_value, $pattern);
            if (!empty($positions)) {
                $result[$page] = $positions;
            }
        }

        if(sizeof($this->pages) > 1){
            foreach($this->pages as $page => $page_value) {
                $allInOne .= $page_value;
                $allInOne .= $this->delimiter;
            }
            $allInOne = trim($allInOne);

            // all
            $positions = $this->findPosition($allInOne, $pattern);
            if (!empty($positions)) {
                $result['all'] = $positions;
            }
        }

        return $result;
    }


    /**
     * Helper method used by the patternSearch() method.
     * @param $where
     * @param $pattern
     * @return array all the starting positions of the searched pattern in the input text.
     */
    private function findPosition($where, $pattern){
        $lastPos = 0;
        $positions = array();

        while (($lastPos = strpos($where, $pattern, $lastPos))!== false) {
            $positions[] = $lastPos + 1;
            $lastPos = $lastPos + strlen($pattern);
        }

        return $positions;
    }


    /**
     * Calculating the index of coincidence.
     * The inputs are set by the constructor.
     * @param $normalize if normalize by the letters count
     * @param $approx if approximation formula should be used
     * @return array the IC.
     */
    public function indexOfCoincidence($normalize, $approx){

       if($approx){
           $ic = $this->indexOfCoincidenceApprox($normalize);
       } else {
           $ic = $this->indexOfCoincidenceDirect($normalize);
       }

       return $ic;

    }


    /**
     * Calculates the index of coincidence with the standard formula IC = 1/(N*(N-1)) * (sum n * (n-1)).
     *  he inputs are set by the constructor.
     * @param $normalize if normalize by the letters count
     * @return array the IC.
     */
    public function indexOfCoincidenceDirect($normalize) {
        $res = array();
        $frequency = $this->getAbsoluteNGrams($this->pages, $this->n, $this->delimiter);

        foreach($frequency as $page => $page_arr) {
            $ic = 0;
            $elements = 0;
            $tot = 0;
            foreach($page_arr as $key => $val) {
                $ic += $val * ($val-1);
                $elements += $val;
                $tot++;
            }
            $ic /= $elements * ($elements -1);
            if($normalize){
                $ic /= $tot;
            }
            $res[$page] = $ic;
        }
        return $res;
    }


    /**
     * Calculates the index of coincidence using an appr. formula IC = pi^2.
     *  The inputs are set by the constructor.
     * @param $normalize if normalize by the letters count
     * @return array the IC.
     */
    public function indexOfCoincidenceApprox($normalize) {
        $res = array();
        $frequency = $this->getRelativeNGrams($this->pages, $this->n, $this->delimiter);

        foreach($frequency as $page => $page_arr) {
            $ic = 0;
            $tot = 0;
            foreach($page_arr as $key => $val) {
//                if($key == 'sum') continue;
                $ic += $val * $val;
                $tot++;
            }
            if($normalize){
                $ic /= $tot;
            }
            $res[$page] = $ic;
        }
        return $res;
    }

    /**
    *  Calculates the Shannon's Entropy.
    *  The inputs are set by the constructor.
    *
    *  Returns the entropy value.
    */
    public function ShannonsEntropy() {
        $res = array();
        $frequency = $this->getRelativeNGrams($this->pages, $this->n, $this->delimiter);

        foreach($frequency as $page => $page_arr) {
            $entropy = 0;
            foreach($page_arr as $key => $val) {
//                if($key == 'sum') continue;
                $entropy -= $val * log($val) / log(2);
            }

            $res[$page] = $entropy;
        }
        return $res;
    }

    /**
    *  Calculates the relative count of all n-grams (in percentage).
    *  The inputs are set by the constructor.
    *
    *  Returns the n-gram values.
    */
    public function getRelativeNGrams() {
        $abs = $this->getAbsoluteNGrams();

        foreach($abs as $page => $page_arr) {
            $sum = 0;
            foreach($page_arr as $key => $val) {
                $sum += $val;
            }
            foreach($page_arr as $key => $val) {
                $relfr = $val / ($sum * 1.0);
                $page_arr[$key] = number_format($relfr,4);
            }
            $abs[$page] = $page_arr;
        }
        return $abs;
    }

    /**
    *  Calculates the absolute count of all n-grams.
    *  The inputs are set by the constructor.
    *
    *  Returns the n-gram values.
    */
    public function getAbsoluteNGrams() {
        $result = array();
        $allInOne = "";

        // by page
        foreach($this->pages as $page => $page_value) {
            $result[$page] = $this->countNGram($page_value);
        }

        if(sizeof($this->pages) > 1){
            foreach($this->pages as $page => $page_value) {
                $allInOne .= $page_value;
                $allInOne .= $this->delimiter;
            }
            $allInOne = trim($allInOne);
            $result['all'] = $this->countNGram($allInOne);
        }


        return $result;
    }


    /**
     * Helper method used by the getAbsoluteNGrams() method.
     * Some inputs are set by the constructor.
     * @param $text input
     * @return array the n-gram values.
     */
    private function countNGram($text){
        $ngrams = array();

        // split by delimitter
        if($this->delimiter == ""){
            $pieces_all = str_split($text);
        } else {
            $pieces_all = explode($this->delimiter, $text);
        }

        //$sum = 0;
        for ($i = 0; $i < count($pieces_all) - ($this->n-1); $i++) {
            $piece = "";
            for($j=0; $j < $this->n; $j++){
                $piece .= $pieces_all[$i+$j];
            }
            $ngrams[$piece] = !array_key_exists($piece, $ngrams) ? 1 :  $ngrams[$piece] + 1;
            //$sum++;
        }

        //$ngrams['sum'] = $sum;
        return $ngrams;
    }

    /**
     * Method used to calculate all contacting pairs (pairs of letters) for all input pages.
     * Some inputs are set by the constructor.
     * @param $text input
     * @return array contacts.
     */
    public function getContacts(){
        $result = array();
        $allInOne = "";

        // by page
        foreach($this->pages as $page => $page_value) {
            $result[$page] = $this->countContacts($page_value);
        }

        if(sizeof($this->pages) > 1){
            foreach($this->pages as $page => $page_value) {
                $allInOne .= $page_value;
                $allInOne .= $this->delimiter;
            }

            $allInOne = trim($allInOne);
            // all pages
            $result["all"] = $this->countContacts($allInOne);
        }



        return $result;
    }

    /**
     * Helper method used by the getContacts() method.
     * Some inputs are set by the constructor.
     * @param $text input
     * @return array contacts.
     */
    private function countContacts($text){
        $ngrams = array();
        $this->n = 2;

        // split by delimiter
        if($this->delimiter == ""){
            $pieces_all = str_split($text);
        } else {
            $pieces_all = explode($this->delimiter, $text);
        }

        for ($i = 0; $i < count($pieces_all) - ($this->n-1); $i++) {
            $piece = $pieces_all[$i] . $pieces_all[$i+1];
            if(ord($pieces_all[$i]) > ord($pieces_all[$i+1])){ // compare the ASCII values
                $piece = strrev($piece);
                // always save in ascending to make it symmetric (BA counts as AB)
            }

            $ngrams[$piece] = !array_key_exists($piece, $ngrams) ? 1 :  $ngrams[$piece] + 1;
        }

        // written in cicrle
        $piece = $pieces_all[$this->n-1] . $pieces_all[0]; // connect last and first
        if(ord($pieces_all[$this->n-1]) > ord($pieces_all[0])){ // compare the ASCII values
            $piece = strrev($piece);
        }
        $ngrams[$piece] = !array_key_exists($piece, $ngrams) ? 1 :  $ngrams[$piece] + 1;

        return $ngrams;
    }




}
