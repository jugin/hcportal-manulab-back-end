<?php
/*
*
* Copyright 2019 Eugen Antal, FEI STU in Bratislava
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Created by PhpStorm.
* User: Jugin
* Date: 22/01/2019
*/

include_once 'textoperations.php';

class Anagrams
{
    // variables
    private $text;
    private $dictionary;

    //constructor
    public function __construct($txt){
        $this->initialize();

        $keepSpace = 0; // remove the spaces and convert to TSA
        $to = new TextOperations();
        if(isset($txt) && $txt != null) {
            $this->text = $to->convertToTelegraphic($txt, $keepSpace);
        }
    }

    //private methods
    private function initialize(){
       // $IC_languages = array();
       // $IC_languages["Italian"] = 0.0738;
       // $this->IC_languages = $IC_languages;
    }

    //public methods

    public function loadDictionary($dict){
        $this->dictionary = $dict;
    }

    public function detectAnagrams($first, $second){
        if(is_string($first) == false || is_string($second) == false){
            return false;
        }

        $to = new TextOperations();

        $first = $to->convertToTelegraphic($first, false);
        $second = $to->convertToTelegraphic($second, false);


        if(strlen($first) != strlen($second)){
            return false;
        }
        // split and sort
        $aParts = str_split($first);
        $bParts = str_split($second);
        sort($aParts);
        sort($bParts);
        for ($i = 0; $i < count($aParts); $i++) {
            if($aParts[i] != $bParts[i]){
                return false;
            }
        }
        return true;
    }

   // TODO add detectAnagrams method using prime factors

}