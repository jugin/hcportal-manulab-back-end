<?php
/*
 *
 * Copyright 2018 Eugen Antal, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 * Date: 07/11/2018
 */

class TextOperations
{

    // constructor without parameters
    public function __construct(){

    }

    /**
     * @param $source - what to replace
     * @param $dest - replace with
     * @param $text - where to replace
     * @return mixed - the replaced text
     */
    public function substitute($source, $dest, $text){
        $replaced = str_replace($source, $dest, $text);
        return $replaced;
    }

    /**
     * Remove accents from the input text. Converts the text into a telegraphic form.
     * @param $text - input text
     * @param $keepSpace - flag set to TRUE when the space should not be removed.
     * @return mixed - the converted text (also in lower case).
     */
    public function convertToTelegraphic($text, $keepSpace){
        $text = $this->convertToLowerCase($text);
        $text = normalizer_normalize($text, Normalizer::FORM_D);
        $pattern = '/[^a-z]/i';
        if($keepSpace){
            $pattern =  '/[^a-z ]/i';
        }
        $text = preg_replace($pattern, '', $text);
        return $text;
    }

    /**
     * Converts a text into a lower-case letters.
     * @param $text - input text
     * @return string - converted text
     */
    public function convertToLowerCase($text){
        return strtolower($text);
    }



}