<?php
/*
 *
 * Copyright 2018-2019 Eugen Antal and Michal Firča, FEI STU in Bratislava
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by PhpStorm.
 * User: Jugin
 * Date: 11/11/2018
 */

include_once 'statistics.php';
include_once 'textoperations.php';

class Cryptanalysis {

    // variables
    private $text;
    private $lang = null;
    private $mono;
    private $bi;

    //private $IC_languages; // private variable replaced with constant (defined at compile time)
    const IC_LANGUAGES = [
        "Italian" => 0.0738,
        "French" => 0.0694,
        "Russian" => 0.0529,
        "English" => 0.0667,
        "German" => 0.0734,
        "Spanish" => 0.0729,
        "Portuguese" => 0.0824,
        "Turkish" => 0.0701,
        "Swedish" => 0.0681,
        "Polish" => 0.0607,
        "Danish" => 0.0672,
        "Icelandic" => 0.0669,
        "Finnish" => 0.0699,
        "Czech" => 0.0510,
        "Latin" => 0.0726, // calculated from rel. fr. available at: https://www.sttmedia.com/characterfrequency-latin
        "Random text" => 0.0385
    ];

    const SUPPORTED_VD_LANG = [
        "French",
        "English",
        "German",
        "Slovak"
    ];

    const SUPPORTED_VD_METHODS = [
        "Sukhotin",
        "VowelSolution",
        //"DigramSolution",
        "ConsonantLine"
    ];

    //constructor
    public function __construct($txt, $lang){
        //
        if(isset($lang)){
            $this->lang = $lang;
        }
        $this->text = $txt;
        //
        $this->initialize();
    }


    //private methods
    private function initialize(){
        if ($this->lang == null || in_array ($this->lang,Cryptanalysis::SUPPORTED_VD_LANG) == false) {
            $this->lang = "English";
        }
        // read local 1 and 2-gram files

        $filename = "https://manulab.hcportal.eu/api/text/objects/ref/" . $this->lang . "_1.csv";
        $theData = file_get_contents($filename);
        $assoc_array = array();
        $my_array = explode("\n", $theData);
        foreach($my_array as $line)
        {
            $tmp = explode(",", $line);
            if (isset($tmp[0]) && isset($tmp[1])) {
                $assoc_array[$tmp[0]] = $tmp[1];
            }
        }
        $this->mono = $assoc_array;

        $filename = "https://manulab.hcportal.eu/api/text/objects/ref/" . $this->lang . "_2.csv";
        $theData = file_get_contents($filename);

        $assoc_array = array();
        $my_array = explode("\n", $theData);
        foreach($my_array as $line)
        {
            $tmp = explode(",", $line);
            if (isset($tmp[0]) && isset($tmp[1])) {
                $assoc_array[$tmp[0]] = $tmp[1];
            }

        }
        $this->bi = $assoc_array;
    }


    //public methods

    /*
     *  Language guessing based on the IC.
     *  The input text is converted to lower case chars
     *  and converted to telegraphic alphabet (without accents and space).
     *
     *  Returns the sorted list of differences of IC vs. rference language IC.
     */
    public function guessTheUsedLanguage()
    {
        $n = 1;
        $delimiter = '';
        $keepSpace = 0;

        $to = new TextOperations();

        $pages = array();
        $pages["all"] = $to->convertToTelegraphic($this->text, $keepSpace);

        $stat = new Statistics($pages, $n, $delimiter);
        $normalize = 0;
        $approximate = 0;
        $ic = $stat->indexOfCoincidence($normalize, $approximate);
        $ic = $ic["all"];

        // compare
        $diffs = array();
        foreach (Cryptanalysis::IC_LANGUAGES as $key => $value) { //$this->IC_languages;
            $diff = abs($value - $ic);
            $diffs[$key] = number_format($diff,4);
        }
        // sort associative arrays in ascending order, according to the value
        asort($diffs);

        $diffs["IC"] = number_format($ic,4);
        $diffs["reference"] = Cryptanalysis::IC_LANGUAGES; //$this->IC_languages;

        return $diffs;
    }

    // TODO
    public function guessTheUsedCipher(){

    }

    /*
     * VOWEL DETECTION METHODS
     * */


    public function vowelDetection($method){
        if ($method != null) {
           switch ($method){
                case  "Sukhotin" : {
                    return $this->vowelDetectionSukhotin();
                } break;
                case "VowelSolution" : {
                    return $this->vowelDetectionVowelSolution();
                } break;
                case "DigramSolution" : {
                    return $this->vowelDetectionDigramSolution();
                } break;
                case "ConsonantLine": {
                    return $this->vowelDetectionConsonantLine();
                } break;
           }
        }

        return "unsupported vowel detection method.";
    }

    public function vowelDetectionSukhotin(){
        // 0. converto to TSA without space
        $n = 1;
        $delimiter = '';
        $keepSpace = 0;

        $to = new TextOperations();
        $pages = array();
        $pages["input"] = $to->convertToTelegraphic($this->text, $keepSpace);

        // 1. calculate the input frequencies
        $stat = new Statistics($pages, $n, $delimiter);

        // 1. calculate the bigrams (neighbors)
        $contacts = $stat->getContacts()["input"];
        // remove pairs (2-grams) with the same value e.g. "aa", "ee"
        foreach ($contacts as $key => $value){
            if($key[0]==$key[1]){
                unset($contacts[$key]);
            }
        }

        // 2. sum the rows (each occurrance of the letter)
        $rowSum = array();
        foreach ($contacts as $key => $value){
            $rowSum[$key[0]] = !array_key_exists($key[0], $rowSum) ? $value :  $rowSum[$key[0]] + $value;
            $rowSum[$key[1]] = !array_key_exists($key[1], $rowSum) ? $value :  $rowSum[$key[1]] + $value;


        }
        //$monograms = $stat->getAbsoluteNGrams()["input"];


        // 3. Sukhotin vowel detection

        $vowels = array();
        do {
            if(max($rowSum) <= 0){
                break;
            }
            //3.1 find the maximum frequent monogram, mark as vowel
            $maxKey = array_keys($rowSum, max($rowSum))[0];

            array_push($vowels, $maxKey);
            $rowSum[$maxKey] = 0; // mark as used

            //3.2 find all neighbors of the max frequent monogram
            foreach ($contacts as $key => $value) {
                // for each pair update the row sum frequency depending on the contacts
                // since "aa" is not allowed, only the half is investigated
                if ($key[0] == $maxKey) {
                    $rowSum[$key[1]] -= 2 * $value;
                } else if($key[1] == $maxKey){
                    $rowSum[$key[0]] -= 2 * $value;
                }
            }
        } while(max($rowSum) > 0);

        return $vowels;
    }

    private function leftContact($pages, $letters)
    {
        $contactLetters = array();
        $stat = new Statistics($pages, 2, '');
        $biGrams = $stat->getAbsoluteNGrams();

        foreach ($letters as $letter) {
            foreach ($biGrams["input"] as $key => $v) {
                if ($letter == $key[1]) {
                    $contactLetters[$key[0]] = !array_key_exists($key[0], $contactLetters) ? 1 :  $contactLetters[$key[0]] + 1;

                }
            }
        }
        return $contactLetters;
    }

    private function rightContact($pages, $letters)
    {
        $contactLetters = array();
        $stat = new Statistics($pages, 2, '');
        $biGrams = $stat->getAbsoluteNGrams();

        foreach ($letters as $letter) {
            foreach ($biGrams["input"] as $key => $v) {
                if ($letter == $key[0]) {
                    $contactLetters[$key[1]] = !array_key_exists($key[1], $contactLetters) ? 1 :  $contactLetters[$key[1]] + 1;
                }
            }
        }
        return $contactLetters;
    }

    /*
     * VowelSolution pointers
     * */

    private function firstPointer($pages){
        $vowels = array();
        $stat = new Statistics($pages,1,'');
        $relativeFrequency = $stat->getRelativeNGrams();
        $frequency = 0;

        // get the most frequent letters while the sum of frequencies is less than 60%
        do{
            $maxKey = array_keys($relativeFrequency["input"], max($relativeFrequency["input"]))[0];
            array_push($vowels, $maxKey);
            $frequency += $relativeFrequency["input"][$maxKey];
            $relativeFrequency["input"][$maxKey] = 0;
        }while ($frequency <= 0.6);

        return $vowels;
    }

    private function secondPointer($pages){
        $vowels = array();
        $lowFrLetters = array();
        $stat = new Statistics($pages,1,'');
        $relativeFrequency = $stat->getRelativeNGrams();
        $frequency = 0;

        //get the less frequent letters
        do{
            $minKey = array_keys($relativeFrequency["input"], min($relativeFrequency["input"]))[0];
            array_push($lowFrLetters, $minKey);
            $frequency += $relativeFrequency["input"][$minKey];
            $relativeFrequency["input"][$minKey] = 100;
        }while ($frequency <= 0.2);

        //... and get/count their contacts
        $vowelsLeft = $this->leftContact($pages,$lowFrLetters);
        $vowelsRight = $this->rightContact($pages,$lowFrLetters);

        // connect left and right contacts
        $finalArray = $vowelsLeft;
        foreach($vowelsRight as $k => $v) {
            if(array_key_exists($k, $finalArray)) {
                $finalArray[$k] += $v;
            } else {
                $finalArray[$k] = $v;
            }
        }

        // get all contacts with frequency > 2%
        foreach ($finalArray as $letter => $value){
            if ($value/strlen($pages["input"]) >= 0.02){
                array_push($vowels, $letter);
            }
        }

        return $vowels;
    }

    private function thirdPointer($pages){
        $vowels = array();
        $stat = new Statistics($pages,1,'');
        $relativeFrequency = $stat->getRelativeNGrams();

        // count the lefr/rigth contact for all letters; count the unique contacts
        foreach ($relativeFrequency["input"] as $key => $value){
            $tmpArray = array();
            array_push($tmpArray, $key);
            $left = $this->leftContact($pages,$tmpArray);
            $right = $this->rightContact($pages,$tmpArray);
            $finalArray = $left;
            foreach($right as $k => $v) {
                if(array_key_exists($k, $finalArray)) {
                } else {
                    $finalArray[$k] = 1;
                }
            }
            $count = sizeof($finalArray);
            $vowels[$key] = $count;
        }

        // get 6 letters with the highest number of unique contacts
        $counter = 0;
        $result = array();
        while($counter < 6 ){
            $maxKey = array_keys($vowels, max($vowels))[0];
            array_push($result,$maxKey);
            $vowels[$maxKey] = 0;
            $counter++;
        }
        return $result;
    }

    private function fourthPointer($pages){
        // repeating bigrams
        $vowels = array();
        $stat = new Statistics($pages, 4, '');
        $tetraGrams = $stat->getRelativeNGrams();
        foreach ($tetraGrams["input"] as $key => $value){
            if ($key[0] == $key[2] and $key[1] == $key[3]){
                array_push($vowels,$key[0]);
                array_push($vowels,$key[1]);
            }
        }
        return $vowels;
    }

    private function fifthPointer($pages) {
        //reversed bigrams
        $vowels = array();
        $stat = new Statistics($pages, 2, '');
        $biGrams = $stat->getRelativeNGrams();
        foreach ($biGrams["input"] as $key => $value) {
            $reverseKey = strrev($key);
            if (array_key_exists($reverseKey, $biGrams["input"])) {
                if (!array_key_exists($key[0],$vowels)){
                    $vowels[$key[0]] = 1;
                } else $vowels[$key[0]]++;
                if (!array_key_exists($key[1],$vowels)) {
                    $vowels[$key[1]] = 1;
                } else $vowels[$key[1]]++;
            }
        }

        $counter = 0;
        $result = array();
        while($counter < 6 ){
            $maxKey = array_keys($vowels, max($vowels))[0];
            array_push($result,$maxKey);
            $vowels[$maxKey] = 0;
            $counter++;
        }
        return $result;
    }

    private function sixthPointer($pages){
        // doubled consonants bordered with vowels
        $vowels = array();
        $stat = new Statistics($pages, 4, '');
        $tetraGrams = $stat->getRelativeNGrams();
        foreach ($tetraGrams["input"] as $key => $value){
            if ($key[1] == $key[2]){
                array_push($vowels,$key[0]);
                array_push($vowels,$key[3]);
            }
        }
        return $vowels;
    }

    private function eightPointer($pages){
        $vowels = array();
        $stat = new Statistics($pages,1,'');
        $relativeFrequency = $stat->getRelativeNGrams();
        $frequency = 0;

        // get the most frequent letters while the sum of frequencies is less than 60%
        $tmpArray = array();
        do{
            $maxKey = array_keys($relativeFrequency["input"], max($relativeFrequency["input"]))[0];
            array_push($tmpArray, $maxKey);
            $frequency += $relativeFrequency["input"][$maxKey];
            $relativeFrequency["input"][$maxKey] = 0;
        }while ($frequency <= 0.6);

        $stat = new Statistics($pages,2,'');
        $relativeFrequency = $stat->getRelativeNGrams();

        // mark all bigrams with frequency > 0.10
        for ($i = 0; $i < count($tmpArray); $i++){
            $firstLetter  = $tmpArray[$i];
            for ($j = 0; $j < count($tmpArray); $j++){
                $secondLetter = $tmpArray[$j];
                if ($i == $j){continue;}
                $digram = $firstLetter.$secondLetter;
                $revDigram = strrev($digram);
                $tmpValue = 0;

                if (array_key_exists($digram,$relativeFrequency)){
                    $tmpValue += $relativeFrequency[$digram] / count($relativeFrequency) * 100;
                }
                if (array_key_exists($revDigram,$relativeFrequency)){
                    $tmpValue += $relativeFrequency[$revDigram] / count($relativeFrequency) * 100;
                }

                if ($tmpValue < 0.10){
                    if (!array_key_exists($firstLetter,$vowels)){
                        $vowels[$firstLetter] = 1;
                    } else $vowels[$firstLetter]++;
                    if (!array_key_exists($secondLetter,$vowels)) {
                        $vowels[$secondLetter] = 1;
                    } else $vowels[$secondLetter]++;
                }
            }
        }

        $result = array();
        foreach ($vowels as $key => $value){
            if ($value < (count($tmpArray)/2)){
                unset($vowels[$key]);
            } else array_push($result,$key);
        }
        return $result;
    }


    public function vowelDetectionVowelSolution(){
        // 0. converto to TSA without space
        $keepSpace = 0;

        $to = new TextOperations();
        $pages = array();
        $pages["input"] = $to->convertToTelegraphic($this->text, $keepSpace);

        // get results from all pointers
        $arrays = array(
            $first = $this->firstPointer($pages),
            $second = $this->secondPointer($pages),
            $third = $this->thirdPointer($pages),
            $fourth = $this->fourthPointer($pages),
            $fifth = $this->fifthPointer($pages),
            $sixth = $this->sixthPointer($pages),
            $eighth = $this->eightPointer($pages),
        );

        // count how many pointers marked a letter as vowel - score the letters
        $arrayOfVowels = array();
        foreach( $arrays as $array){
            foreach ($array as $value){
                if (!array_key_exists($value, $arrayOfVowels)){
                    $arrayOfVowels[$value] = 1;
                } else $arrayOfVowels[$value]++;
            }
        }

        // result should be the 6 letters with the highest score (marked as vowel more times)
        $counter = 0;
        $result = array();
        while($counter < 6 ){
            $maxKey = array_keys($arrayOfVowels, max($arrayOfVowels))[0];
            array_push($result,$maxKey);
            $arrayOfVowels[$maxKey] = 0;
            $counter++;
        }

        return $result;
    }


    public function vowelDetectionDigramSolution(){
        $result = array();
        $keepSpace = 0;

        $to = new TextOperations();
        $pages = array();
        $pages["input"] = $to->convertToTelegraphic($this->text, $keepSpace);

        $stat = new Statistics($pages,1 , '');
        $monoGrams = $stat->getRelativeNGrams();
        $stat = new Statistics($pages, 2, '');
        $biGrams = $stat->getRelativeNGrams();

        // fill array with keys of most frequent from reference values
        $arrayOfSearchBigrams = array();
        $tmp = 0;

        foreach ($this->bi as $key => $value){
            $tmp++;
            array_push($arrayOfSearchBigrams,$key);
            if ($tmp == 10) break;
        }

        $numberOfTests = 0;
        while ($numberOfTests < 10){
            $maxDigram = array_search(max($biGrams["input"]), $biGrams["input"]);
            $firstLetter = $maxDigram[0];
            $secondLetter = $maxDigram[1];
            $dist = array();
            $co = array();
            $naco = array();

            $tmpCounter = 0;
            foreach ($arrayOfSearchBigrams as $digram){
                $revDigram = strrev($digram);

                $digValue = $this->bi[$revDigram];
                $digValueRev = $this->bi[$digram];

                $frequency = $biGrams["input"][$maxDigram]/count($biGrams);
                if (array_key_exists(strrev($maxDigram), $biGrams)) {
                    $frequencyRev = $biGrams["input"][strrev($maxDigram)] / count($biGrams);
                } else $frequencyRev = 0;
                $firstLetterFreq = $monoGrams["input"][$firstLetter];
                $secondLetterFreq = $monoGrams["input"][$secondLetter];
                $firstLetterFreqFr = $this->mono[$digram[0]];
                $secondLetterFreqFr = $this->mono[$digram[1]];

                $biDiff = abs($frequency - $digValue);
                $biDiffRev = abs($frequencyRev - $digValueRev);
                $firstLetterDiff = abs($firstLetterFreq - $firstLetterFreqFr);
                $secondLetterDiff = abs($secondLetterFreq - $secondLetterFreqFr);
                $w1 = 2;
                $w2 = 1;
                $w3 = 2;
                $w4 = 2;
                $sum = ($biDiff*$w1)+($biDiffRev*$w2)+($firstLetterDiff*$w3)+($secondLetterDiff*$w4);
                $dist[$tmpCounter] = $sum;
                $co[$tmpCounter] = $maxDigram;
                $naco[$tmpCounter] = $digram;
                $tmpCounter++;
            }

            $keyOfminValue = array_search(min($dist),$dist);
            $result[$co[$keyOfminValue]] = $naco[$keyOfminValue];
            $biGrams["input"][$maxDigram] = 0;
            $numberOfTests++;
        }
        return $result;
    }

   /*
    * ConsonantLine helper method
    * */
    private function varietyOfContact($pages){
        $contactsOfLetters = array();
        $stat = new Statistics($pages, 2, '');
        $diGrams = $stat->getAbsoluteNGrams();
        foreach ($diGrams["input"] as $key => $value) {
            if (!array_key_exists($key[0], $contactsOfLetters)) {
                $contactsOfLetters[$key[0]] = array();
                array_push($contactsOfLetters[$key[0]], $key[1]);
            } else {
                if (!array_key_exists($key[1], $contactsOfLetters[$key[0]])){
                    array_push($contactsOfLetters[$key[0]],$key[1]);
                }
            }
        }

        foreach ($contactsOfLetters as $key => $value){
            $contactsOfLetters[$key] = count($value);
        }
        return $contactsOfLetters;
    }

    public function vowelDetectionConsonantLine(){
        $result = array();
        $keepSpace = 0;

        $to = new TextOperations();
        $pages = array();
        $pages["input"] = $to->convertToTelegraphic($this->text, $keepSpace);

        $stat = new Statistics($pages,1 , '');
        $monoGrams = $stat->getAbsoluteNGrams();
        $varietyOfContacts = $this->varietyOfContact($pages);
        $sumOfVar = array_sum($varietyOfContacts);

        asort($varietyOfContacts);

        $count = 0;
        $consonants = array();
        foreach ($varietyOfContacts as $key => $value){
            $count +=$value;
            if ($count > $sumOfVar*0.2){
                break;
            }
            array_push($consonants, $key);
        }

        $tmp = array_keys($varietyOfContacts);
        $tmpValue = 1;
        for ($i = count($consonants); $i < count($monoGrams); $i++){
            if ($varietyOfContacts[$tmp[$i]] == $varietyOfContacts[$tmp[$i+1]]){
                $tmpValue++;
            } else break;
        }

        for ($i = 0; $i < $tmpValue; $i++){
            if ($varietyOfContacts[$tmp[count($consonants)+1]] -  $monoGrams["input"][$tmp[count($consonants)+1]] < 0){
                array_push($consonants, $tmp[count($consonants)+1]);
            }
        }

        $pozFromLeft = $this->leftContact($pages, $consonants);
        $pozFromRight = $this->rightContact($pages, $consonants);

        foreach ($pozFromLeft as $key => $value){
            if (!array_key_exists($key, $result)){
                $result[$key] = $value;
            } else $result[$key] +=$value;
        }

        foreach ($pozFromRight as $key => $value){
            if (!array_key_exists($key, $result)){
                $result[$key] = $value;
            } else $result[$key] +=$value;
        }

        $counter = 0;
        $finalResult = array();
        while($counter < 6 ){
            $maxKey = array_keys($result, max($result))[0];
            array_push($finalResult,$maxKey);
            $result[$maxKey] = 0;
            $counter++;
        }

        return $finalResult;
    }


}
