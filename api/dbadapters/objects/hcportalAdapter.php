<?php
/*
*
* Copyright 2019 Eugen Antal, FEI STU in Bratislava
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Created by PhpStorm.
* User: Jugin
* Date: March 2019
*/

class HCPortalAdapter
{
    // variables
    private $records;

    //constructor
    public function __construct()
    {
        $this->initialize();
    }

    //private methods
    private function initialize()
    {
        $this->records = array();
    }


    public function downloadTextFiles()
    {
        return $this->downloadFromHCPortal("text");
    }

    public function downloadImageFiles()
    {
        return $this->downloadFromHCPortal("image");
    }

    private function downloadFromHCPortal($filetype)
    {
        $records = array();

        $url = 'http://cryptograms.hcportal.eu/api/rest/getCryptogramRecordsForManulab.php';
        $opts = array(
            'http' => array(
                'method' => "GET"
            )
        );

        $context = stream_context_create($opts);

        $file = file_get_contents($url, false, $context);

        $json_data = json_decode($file);
        if (isset($json_data->records) && (sizeof($json_data->records) > 0)) {

            foreach ($json_data->records as $record_item) {
                if (isset($record_item->data) && (sizeof($record_item->data) > 0)) {

                    foreach ($record_item->data as $group_data_item) {
                        if ($group_data_item->filetype == $filetype) {
                            $item = array();
                            $item['source'] = 'HCPORTAL';
                            $item['name'] = $record_item->name . ' - ' . $group_data_item->description;
                            // $item['name'] = $record_item->name . ' - ' . $group_data_item->group . ' - ' . $group_data_item->description;
                            if ($filetype === "text") {
                                $item['text'] = $group_data_item->blobb;
                            } else if($filetype === "image") {
                                $item['url'] = $group_data_item->blobb;
                            } else {
                                break;
                            }

                            array_push($records, $item);
                        }

                    }
                }
            } // for each record item
        }
        return $records;
    }

}