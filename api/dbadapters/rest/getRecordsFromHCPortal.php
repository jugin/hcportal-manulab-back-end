<?php
/*
*
* Copyright 2019 Eugen Antal, FEI STU in Bratislava
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Created by PhpStorm.
* User: Jugin
* Date: March 2019
*/

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
header("Content-Type: application/json; charset=UTF-8");

include_once '../objects/hcportalAdapter.php';

if (isset($_GET['type'])) {
    $type = $_GET['type'];
    $adapter = new HCPortalAdapter();
    $records;
    if ($type === "text") {
        $records = $adapter->downloadTextFiles();
    } else if ($type === "image") {
        $records = $adapter->downloadImageFiles();
    } else {
        http_response_code(412); // precondition failed code
        echo json_encode(
            array("message" => "Input error. Invalid queryParam - type. Only text or image is allowed.", "?type=",$type)
        );
        die();
    }

    http_response_code(200);
    echo json_encode($records, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

} else {
    http_response_code(412); // precondition failed code
    echo json_encode(
        array("message" => "Input error. QueryParam type not set.")
    );
    die();
}

?>