# HCPortal ManuLab online back-end

Part of the [HCPortal](http://hcportal.eu) project (Portal of Historical Ciphers). 

ManuLab is a software product for statistical analysis of encrypted historical manuscripts. The document analysis is performed via a chain of filters (main building elements). A filter represents any operation realizable on a document transcription divided into a set of pages. 

ManuLab API is a ported version of the [ManuLab](https://bitbucket.org/jugin/manulab) functionality (back-end) to PHP scripts (API).

# Copyright
Copyright 2018-2022 HCPortal team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this project except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
